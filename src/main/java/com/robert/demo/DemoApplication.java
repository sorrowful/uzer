package com.robert.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

    // entry point for the application
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
