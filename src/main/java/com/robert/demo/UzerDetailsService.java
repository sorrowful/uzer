package com.robert.demo;

import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.*;

@Service
public class UzerDetailsService implements UserDetailsService {

    private final UzerRepository repository;

    UzerDetailsService(UzerRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        Uzer uzer = repository.findByName(name).orElseThrow(() -> new UzerNotFoundException(name));

        // any user that exists in the database will be assiged their roles
        Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(uzer.getRole()));

        return new User(uzer.getName() + "", uzer.getPassword(), authorities);
    }
}
