package com.robert.demo;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
class Uzer {

    private @Id @GeneratedValue Long id;

    @Column(unique = true)
    private String name;

    private String email;
    private String phone;
    private String password;
    private String role;

    Uzer() {
    }

    Uzer(String name, String email, String phone, String password, String role) {

        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.role = role;
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getRole() {
        return this.role;
    }

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public String getPassword() {
        return this.password;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Uzer))
            return false;
        Uzer uzer = (Uzer) o;
        return Objects.equals(this.id, uzer.id) && Objects.equals(this.name, uzer.name)
                && Objects.equals(this.email, uzer.email) && Objects.equals(this.phone, uzer.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name, this.email);
    }

    @Override
    public String toString() {
        return "Uzer{" + "id='" + this.id + '\'' + ", name='" + this.name + '\'' + ", email='" + this.email + '\''
                + ", phone='" + this.phone + '\'' + this.role + '\'' + '}';
    }
}
