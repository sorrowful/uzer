package com.robert.demo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

interface UzerRepository extends JpaRepository<Uzer, Long> {

    public Optional<Uzer> findByName(String name);

}
