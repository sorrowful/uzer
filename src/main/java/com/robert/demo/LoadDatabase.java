package com.robert.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Autowired
    PasswordEncoder passwordEncoder;

    @Bean
    CommandLineRunner initDatabase(UzerRepository repository) {

        // create some sample users
        return args -> {
            log.info("Preloading " + repository.save(new Uzer("goblin", "goblin@hotmail.com", "+1-212-123-4567",
                    passwordEncoder.encode("ialsolovemj"), "ROLE_ADMIN")));
            log.info("Preloading " + repository.save(new Uzer("spidey", "spiderman@gmail.com", "+1-212-224-7654",
                    passwordEncoder.encode("ilovemj"), "ROLE_USER")));
        };
    }
}
