package com.robert.demo;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.core.*;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;

@RestController
class UzerController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    private final UzerRepository repository;

    UzerController(UzerRepository repository) {
        this.repository = repository;
    }

    // get any user
    @GetMapping("/api/users")
    List<Uzer> all() {
        return repository.findAll();
    }

    // register a user (set the role as user to start, can update to admin later)
    @PostMapping("/api/register")
    Uzer newUzer(@RequestBody Uzer newUzer) {
        // don't want to create a user that already exists
        if (repository.findByName(newUzer.getName()).isPresent()) {
            throw new UzerExistsException(newUzer.getName());
        }
        newUzer.setPassword(passwordEncoder.encode(newUzer.getPassword()));
        newUzer.setRole("ROLE_USER");
        return repository.save(newUzer);
    }

    // get a user
    @GetMapping("/api/users/{name}")
    Uzer one(@PathVariable String name) {
        return repository.findByName(name).orElseThrow(() -> new UzerNotFoundException(name));
    }

    @PutMapping("/api/users/{name}")
    // only an admin can update any user, or a user can update themself
    @PostAuthorize("hasRole('ADMIN') or #name == principal.username")
    Uzer replaceUzer(@RequestBody Uzer newUzer, @PathVariable String name) {
        return repository.findByName(name).map(uzer -> {
            uzer.setName(newUzer.getName());
            uzer.setEmail(newUzer.getEmail());
            uzer.setPhone(newUzer.getPhone());
            uzer.setPassword(passwordEncoder.encode(newUzer.getPassword()));

            // only an admin can update a user's role
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_ADMIN"))) {
                uzer.setRole(newUzer.getRole());
            }
            return repository.save(uzer);
        })
                // can't find your user so throw an exception
                .orElseThrow(() -> new UzerNotFoundException(name));
    }

    @DeleteMapping("api/users/{id}")
    void deleteUzer(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
