package com.robert.demo;

class UzerNotFoundException extends RuntimeException {

    UzerNotFoundException(String name) {
        super("Could not find uzer: " + name);
    }
}
