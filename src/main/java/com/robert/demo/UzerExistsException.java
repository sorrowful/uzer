package com.robert.demo;

class UzerExistsException extends RuntimeException {

    UzerExistsException(String name) {
        super("Uzer exists: " + name);
    }
}
