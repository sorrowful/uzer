#!/bin/sh
curl -X GET -u spidey:ilovemj localhost:8080/api/users | jq
curl -X GET -u goblin:ialsolovemj localhost:8080/api/users | jq
curl -X GET -u goblin:ialsolovemj localhost:8080/api/users/goblin | jq
curl -X GET -u homer:ilovemarge localhost:8080/api/users/homer | jq
curl -X GET -u homer:ilovemaggy localhost:8080/api/users/homer | jq
curl -X GET -u homer:ilovemarge localhost:8080/api/users/ | jq
curl -X GET -u homer:ilovemaggy localhost:8080/api/users/ | jq
curl -X GET -u goblin:ialsolovemj localhost:8080/api/users/someonelse | jq
