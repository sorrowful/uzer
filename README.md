```
██╗   ██╗███████╗███████╗██████╗ 
██║   ██║╚══███╔╝██╔════╝██╔══██╗
██║   ██║  ███╔╝ █████╗  ██████╔╝
██║   ██║ ███╔╝  ██╔══╝  ██╔══██╗
╚██████╔╝███████╗███████╗██║  ██║
 ╚═════╝ ╚══════╝╚══════╝╚═╝  ╚═╝
```
uzer is a small project for testing out spring boot.

## pre-requisites

this project was created with gradle 7 and java 11 so it would be good to have those.  it uses postgres for a database and you can run an instance of it using docker.  a `docker-compose.yml` file is included and you can start the database like so:

```bash
docker-compose up -d
```

## building

uzer uses gradle, to build the project:

```bash
./gradlew bootJar
```

## running

you may run this joyous experience by typing the following:

```bash
java -jar build/libs/demo-0.0.1-SNAPSHOT.jar
```

## testing

the tests have some good tests and some bad tests to show error handling, authentication and authorization and such.

to register a user:
```bash
./register.sh
```

to list users:
```bash
./users.sh
```

to update a user:
```bash
./update.sh
```

## license
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)

